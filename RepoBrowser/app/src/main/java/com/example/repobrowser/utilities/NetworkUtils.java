package com.example.repobrowser.utilities;

import android.content.Context;
import android.net.ConnectivityManager;

public class NetworkUtils
{
    public static  boolean isConnectedToInterNet(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm!= null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected())
        {
            return true;
        }

        return false;

    }
}
