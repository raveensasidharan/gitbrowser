package com.example.repobrowser.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.repobrowser.R;
import com.example.repobrowser.model.FilterModel;
import com.example.repobrowser.view.activity.RepoSearchActivity;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

public class RepoFilterFragment extends BottomSheetDialogFragment
{
    private onFilterChangeListener mListener;
    private TextView mSubmittTV;

    private MaterialSpinner mLicenseSpinner;
    private MaterialSpinner mLanguageSpinner;
    private MaterialSpinner mStarSpinner;
    private MaterialSpinner mForkSpinner;
    private MaterialSpinner mSizeSpinner;

    private FilterModel mFilterModel;
    private Activity mActivity;

    public interface onFilterChangeListener
    {
        void onFilterSelected(FilterModel filter);
    }


    public RepoFilterFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (RepoSearchActivity) context;
        mActivity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_repo_filter, container, false);
        mFilterModel    = new FilterModel();

        initView(view);

        return view;
    }


    private void initView(View view)
    {

        mForkSpinner        = view.findViewById(R.id.forkSpinner);
        mForkSpinner.setItems("Select", "100", "250", "500", "1000", "2500", "5000", "10000");
        mForkSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(position > 0)
                     mFilterModel.setmForkCount(Integer.parseInt(item));
                else
                    mFilterModel.setmForkCount(0);
            }
        });

        mLanguageSpinner    = view.findViewById(R.id.languageSpinner);
        mLanguageSpinner.setItems("Select", "Java", "Android", "ReactNative", "Ruby","Python");
        mLanguageSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(position > 0)
                    mFilterModel.setmLanguageString(item);
                else
                    mFilterModel.setmLanguageString(null);

            }
        });

        mLicenseSpinner     = view.findViewById(R.id.licenseSpinner);
        mLicenseSpinner.setItems("Select", "apache-2.0", "bs1-1.0", "mit");
        mLicenseSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(position > 0)
                    mFilterModel.setmLicenseString(item);
                else
                    mFilterModel.setmLicenseString(null);

            }
        });

        mStarSpinner        = view.findViewById(R.id.startSpinner);
        mStarSpinner.setItems("Select", "100", "250", "500", "1000", "2500", "5000", "10000");
        mStarSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(position > 0)
                    mFilterModel.setmStarCount(Integer.parseInt(item));
                else
                    mFilterModel.setmStarCount(0);

            }
        });

        mSizeSpinner        = view.findViewById(R.id.sizeSpinner);
        mSizeSpinner.setItems("Select", "10", "25", "50", "100", "250", "500", "10000");
        mSizeSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(position > 0)
                    mFilterModel.setmSizeCount(Integer.parseInt(item));
                else
                    mFilterModel.setmSizeCount(0);

            }
        });


        mSubmittTV          = view.findViewById(R.id.submitButton);
        mSubmittTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFilterSelected(mFilterModel);
                dismiss();
            }
        });

    }

}
