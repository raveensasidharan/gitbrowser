package com.example.repobrowser.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.repobrowser.R;
import com.example.repobrowser.model.RepoItem;
import com.example.repobrowser.view.activity.RepoDetailsActivity;

import java.util.List;

public class RepoRecyclerAdapter extends RecyclerView.Adapter<RepoRecyclerAdapter.RepoViewHolder>
{
    private  final static String TAG =   RepoRecyclerAdapter.class.getSimpleName();

    private List<RepoItem> mRepoItemList;
    private Context mContext;

    public class RepoViewHolder extends RecyclerView.ViewHolder
    {
        private View mView;
        private ImageView mAvatarView;
        private TextView mNameTV;
        private TextView mFullNameTV;
        private TextView mForkCountTV;
        private TextView mWatchCountTV;
        private TextView mStagerCountTV;

        public RepoViewHolder(View itemView)
        {
            super(itemView);
            mView           =   itemView;
            mNameTV         =   itemView.findViewById(R.id.nameTV);
            mFullNameTV     =   itemView.findViewById(R.id.fullNameTV);
            mForkCountTV    =   itemView.findViewById(R.id.forkTV);
            mWatchCountTV   =   itemView.findViewById(R.id.watcherTV);
            mStagerCountTV  =   itemView.findViewById(R.id.starTV);
            mAvatarView     =   itemView.findViewById(R.id.avatarImage);
        }
    }


    public RepoRecyclerAdapter(Context context, List<RepoItem> repoItemList)
    {
        this.mContext       = context;
        this.mRepoItemList  = repoItemList;

    }


    @NonNull
    @Override
    public RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_repo_item, parent, false);

        return new RepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoViewHolder holder, final int position)
    {
        holder.mNameTV.setText(mRepoItemList.get(position).getmName());
        holder.mForkCountTV.setText(mRepoItemList.get(position).getmForkCount());
        holder.mFullNameTV.setText(mRepoItemList.get(position).getmFullName());
        holder.mWatchCountTV.setText(mRepoItemList.get(position).getmWatcherCount());
        holder.mStagerCountTV.setText(mRepoItemList.get(position).getmStagerCount());
        Glide.with(mContext)
                .load(mRepoItemList.get(position).getmOwner().getmImageUrl())
                .into(holder.mAvatarView);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent   = new Intent(mContext, RepoDetailsActivity.class);
                Bundle  bundle  = new Bundle();

                bundle.putSerializable("repoItem", mRepoItemList.get(position));
                intent.putExtras(bundle);

                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        if (mRepoItemList.size() == 0)
            return 0;
        else
            return mRepoItemList.size();
    }
}
