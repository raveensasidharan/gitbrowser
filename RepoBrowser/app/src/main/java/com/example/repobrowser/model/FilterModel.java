package com.example.repobrowser.model;

public class FilterModel
{
    private String mDateString;
    private String mLanguageString;
    private String mLicenseString;
    private int mForkCount;
    private int mStarCount;
    private int mSizeCount;

    public void setmDateString(String mDateString) {
        this.mDateString = mDateString;
    }

    public String getmDateString() {
        return mDateString;
    }

    public void setmForkCount(int mForkCount) {
        this.mForkCount = mForkCount;
    }

    public int getmForkCount() {
        return mForkCount;
    }

    public void setmLanguageString(String mLanguageString) {
        this.mLanguageString = mLanguageString;
    }

    public String getmLanguageString() {
        return mLanguageString;
    }

    public void setmLicenseString(String mLicenseString) {
        this.mLicenseString = mLicenseString;
    }

    public String getmLicenseString() {
        return mLicenseString;
    }

    public void setmStarCount(int mStarCount) {
        this.mStarCount = mStarCount;
    }

    public int getmStarCount() {
        return mStarCount;
    }

    public void setmSizeCount(int mSizeCount) {
        this.mSizeCount = mSizeCount;
    }

    public int getmSizeCount() {
        return mSizeCount;
    }
}
