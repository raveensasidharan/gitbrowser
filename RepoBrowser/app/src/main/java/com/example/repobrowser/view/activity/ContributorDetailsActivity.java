package com.example.repobrowser.view.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.repobrowser.R;
import com.example.repobrowser.adapter.ContributorListAdapter;
import com.example.repobrowser.adapter.RepoRecyclerAdapter;
import com.example.repobrowser.controller.GitRepoController;
import com.example.repobrowser.model.ContributorResponse;
import com.example.repobrowser.model.RepoItem;

import java.util.List;

public class ContributorDetailsActivity extends AppCompatActivity
{
    private final static String TAG = ContributorDetailsActivity.class.getSimpleName();


    private ImageView mProfileImageView;
    private TextView  mProfileText;

    private RecyclerView mRepoRecycler;
    private RepoRecyclerAdapter mRepoAdapter;

    private ContributorResponse mContributor;
    private GitRepoController mRepoController;
    private List<RepoItem> mRepoItemList;

    private ProgressDialog mProgressDialogue;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributor_details);

        mRepoController   =   new GitRepoController(this);
        if (getIntent().getExtras() != null)
        {
            mContributor    = (ContributorResponse) getIntent().getExtras().getSerializable("contributor");
        }

        initView();
        initRepoDetails();
    }


    private void showProgressDialogue()
    {
        mProgressDialogue = new ProgressDialog(this);
        mProgressDialogue.setMessage("Loading..");
        mProgressDialogue.setCanceledOnTouchOutside(false);
        mProgressDialogue.show();

    }

    private void hideProgressDialogue()
    {
        if (null != mProgressDialogue)
        {
            mProgressDialogue.dismiss();
            mProgressDialogue  =  null;
        }
    }
    private void initRepoDetails()
    {
        showProgressDialogue();
        mRepoController.hitContributorRepoDetails(mContributor.getmRepoUrl(), new GitRepoController.OnUserRepoListener() {
            @Override
            public void onUserRepoListReceived(List<RepoItem> repoItemList) {
                mRepoItemList   = repoItemList;

                mRepoAdapter    = new RepoRecyclerAdapter(ContributorDetailsActivity.this, repoItemList);
                mRepoRecycler.setAdapter(mRepoAdapter);

                hideProgressDialogue();
            }
        });
    }

    private void initView()
    {
        mProfileText    = findViewById(R.id.profileName);
        mProfileText.setText(mContributor.getmName());

        mProfileImageView = findViewById(R.id.profileImage);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        RelativeLayout.LayoutParams params  = new RelativeLayout.LayoutParams(displaymetrics.widthPixels, displaymetrics.widthPixels);
        mProfileImageView.setLayoutParams(params);

        Glide.with(this)
                .load(mContributor.getmImageUrl())
                .into(mProfileImageView);

        mRepoRecycler   = findViewById(R.id.repoRecycler);
        mRepoRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

    }
}
