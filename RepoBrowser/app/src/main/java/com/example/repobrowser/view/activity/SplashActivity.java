package com.example.repobrowser.view.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.example.repobrowser.R;

public class SplashActivity extends AppCompatActivity
{
    private TextView mAppTitleView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mAppTitleView   = findViewById(R.id.appTitle);
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in_animation);
        mAppTitleView.startAnimation(startAnimation);

        startRepoActivity();
    }


    ///
    //start activity after 1sec
    ///
    private void startRepoActivity()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, RepoSearchActivity.class);
                startActivity(intent);
            }
        }, 2000);
    }

}
