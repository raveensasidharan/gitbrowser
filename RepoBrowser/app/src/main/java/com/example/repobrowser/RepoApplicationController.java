package com.example.repobrowser;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class RepoApplicationController extends Application
{
    private static RepoApplicationController sInstance;
    private RequestQueue mRequestQueue;
    @Override
    public void onCreate()
    {
        super.onCreate();

    }

    public static RepoApplicationController getInstance()
    {
        if(sInstance==null)
        {
            sInstance=new RepoApplicationController();
        }
        return sInstance;

    }


    public RequestQueue getmRequestQueue(Context context)
    {
        if(null == mRequestQueue)
        {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return mRequestQueue;
    }

}
