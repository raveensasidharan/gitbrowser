package com.example.repobrowser.view.activity;

import android.app.ProgressDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;

import com.example.repobrowser.R;
import com.example.repobrowser.adapter.RepoRecyclerAdapter;
import com.example.repobrowser.controller.GitRepoController;
import com.example.repobrowser.model.FilterModel;
import com.example.repobrowser.model.GitSearchModel;
import com.example.repobrowser.model.RepoItem;
import com.example.repobrowser.utilities.SearchQueryUtils;
import com.example.repobrowser.view.fragment.RepoFilterFragment;

import java.util.List;

public class RepoSearchActivity extends AppCompatActivity
implements RepoFilterFragment.onFilterChangeListener,SearchView.OnQueryTextListener {

    private final static String TAG = RepoSearchActivity.class.getSimpleName();

    private SearchView mRepoSearchView;
    private FloatingActionButton mFilterButton;
    private RecyclerView mRepoRecyclerView;
    private RepoRecyclerAdapter mRepoAdapter;
    private GitRepoController mRepoController;

    private GitSearchModel mSearchModel;
    private FilterModel mFilterModel;
    private List<RepoItem> mRepoItemList;

    private ProgressDialog mProgressDialogue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_search);

        mRepoController = new GitRepoController(this);
        initView();

        mSearchModel    = new GitSearchModel();
        mSearchModel.setQString("popular");
        mSearchModel.setOrderString("desc");
        mSearchModel.setmPageNumber(1);
        mSearchModel.setmPageCOunt(10);

        mFilterModel    = new FilterModel();

        initWithTrendingRepo(SearchQueryUtils.returnQueryString(mSearchModel, null));
    }



    private void initView()
    {

        mFilterButton       = findViewById(R.id.filterButton);
        mFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilterFragment();
            }
        });

        mRepoRecyclerView   = findViewById(R.id.repoRecycler);
        mRepoRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        mRepoSearchView     = findViewById(R.id.searchView);
        mRepoSearchView.setOnQueryTextListener(this);
    }

    private void initWithTrendingRepo(String queryString)
    {
        showProgressDialogue();

        mRepoController.hitSearchUrl(queryString, 1, new GitRepoController.OnSearchDetailListener() {
            @Override
            public void onRepoListReceived(List<RepoItem> repoItemList) {
                Log.e(  "test", "requst rcvd");
                mRepoItemList   = repoItemList;
                mRepoAdapter    = new RepoRecyclerAdapter(RepoSearchActivity.this, repoItemList);
                mRepoRecyclerView.setAdapter(mRepoAdapter);

                hideProgressDialogue();
            }
        });


    }


    private void showFilterFragment()
    {
        RepoFilterFragment fragment = new RepoFilterFragment();
        fragment.show(getSupportFragmentManager(),"filter");
    }

    private void showProgressDialogue()
    {
        mProgressDialogue = new ProgressDialog(this);
        mProgressDialogue.setMessage("Loading..");
        mProgressDialogue.setCanceledOnTouchOutside(false);
        mProgressDialogue.show();

    }

    private void hideProgressDialogue()
    {
        if (null != mProgressDialogue)
        {
            mProgressDialogue.dismiss();
            mProgressDialogue  =  null;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.e(  "test", "onQueryTextSubmit send"+query);
        mSearchModel.setQString(query);
        initWithTrendingRepo(SearchQueryUtils.returnQueryString(mSearchModel, null));
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onFilterSelected(FilterModel filter)
    {
        mFilterModel    = filter;
        initWithTrendingRepo(SearchQueryUtils.returnQueryString(mSearchModel, mFilterModel));
    }
}
