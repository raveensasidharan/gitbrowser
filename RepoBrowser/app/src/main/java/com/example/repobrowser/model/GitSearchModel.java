package com.example.repobrowser.model;

import java.io.Serializable;

public class GitSearchModel implements Serializable
{
    private String mQString;
    private String mSortString;
    private String mOrderString;
    private int mPageCOunt;
    private int mPageNumber;

    public String getOrderString() {
        return mOrderString;
    }

    public String getQString() {
        return mQString;
    }

    public String getSortString() {
        return mSortString;
    }

    public void setOrderString(String orderString) {
        this.mOrderString = orderString;
    }

    public void setQString(String qString) {
        this.mQString = qString;
    }

    public void setSortString(String sortString) {
        this.mSortString = sortString;
    }

    public void setmPageCOunt(int mPageCOunt) {
        this.mPageCOunt = mPageCOunt;
    }

    public int getmPageCOunt() {
        return mPageCOunt;
    }

    public void setmPageNumber(int mPageNumber) {
        this.mPageNumber = mPageNumber;
    }

    public int getmPageNumber() {
        return mPageNumber;
    }
}
