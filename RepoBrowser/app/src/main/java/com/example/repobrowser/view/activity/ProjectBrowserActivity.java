package com.example.repobrowser.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import com.example.repobrowser.R;

public class ProjectBrowserActivity extends AppCompatActivity
{
    private final static String TAG = ProjectBrowserActivity.class.getSimpleName();

    private String mUrlString;

    private ImageView mBackButton;
    private WebView mBrowserView;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_browser);

        mUrlString  = getIntent().getStringExtra("url");

        mBackButton     =   findViewById(R.id.backButton);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBrowserView    =   findViewById(R.id.browserView);
        WebSettings webSettings = mBrowserView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mBrowserView.loadUrl(mUrlString);
    }
}
