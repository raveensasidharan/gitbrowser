package com.example.repobrowser.utilities;

import com.example.repobrowser.model.FilterModel;
import com.example.repobrowser.model.GitSearchModel;

public class SearchQueryUtils
{
    public static String returnQueryString(GitSearchModel searchModel, FilterModel filterModel)
    {
        String query = "";
        query += searchModel.getQString()+ "&"+
                "per_page="+ searchModel.getmPageCOunt()+ "&"+
                "page="+searchModel.getmPageNumber();

        if (null !=filterModel)
        {
            query   = "";
            query += searchModel.getQString();

            if (0 > filterModel.getmForkCount())
                query += "+"+ "forks:<"+filterModel.getmForkCount();

            if (0 > filterModel.getmStarCount())
                query += "+"+ "stars:<"+filterModel.getmStarCount();

            if (0 > filterModel.getmSizeCount())
                query += "+"+ "size:<"+filterModel.getmSizeCount();

            if (null != filterModel.getmLanguageString())
                query += "+"+ "language:"+filterModel.getmLanguageString();

            if (null != filterModel.getmLicenseString())
                query += "+"+ "license:"+filterModel.getmLicenseString();
            query += "&"+
                    "per_page="+ searchModel.getmPageCOunt()+ "&"+
                    "page="+searchModel.getmPageNumber();

        }


        return query;

    }
}
