package com.example.repobrowser.model;

import org.json.JSONObject;

import java.io.Serializable;

public class OwnerResponse implements Serializable
{
    private String mUserName;
    private String mImageUrl;
    private String mApiUrl;
    private String mHtmUrl;
    private String mRepoUrl;

    public OwnerResponse(JSONObject ownerJson)
    {
        mUserName   = ownerJson.optString("login");
        mImageUrl   = ownerJson.optString("avatar_url");
        mApiUrl     = ownerJson.optString("url");
        mHtmUrl     = ownerJson.optString("url");
        mRepoUrl    = ownerJson.optString("repos_url");
    }

    public String getmApiUrl() {
        return mApiUrl;
    }

    public String getmHtmUrl() {
        return mHtmUrl;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public String getmRepoUrl() {
        return mRepoUrl;
    }

    public String getmUserName() {
        return mUserName;
    }
}
