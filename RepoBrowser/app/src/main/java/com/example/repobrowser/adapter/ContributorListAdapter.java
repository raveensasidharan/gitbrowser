package com.example.repobrowser.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.repobrowser.R;
import com.example.repobrowser.model.ContributorResponse;
import com.example.repobrowser.view.activity.ContributorDetailsActivity;
import com.example.repobrowser.view.activity.RepoDetailsActivity;

import org.w3c.dom.Text;

import java.util.List;

public class ContributorListAdapter extends RecyclerView.Adapter<ContributorListAdapter.ContributorHolder>
{
    private Context mContext;
    private List<ContributorResponse> mContributorList;

    public class ContributorHolder extends RecyclerView.ViewHolder
    {
        private View mView;
        private TextView mContributorName;
        private TextView mContributionCount;
        private ImageView mAvatarImageView;

        public ContributorHolder(View itemView)
        {
            super(itemView);
            mView   =   itemView;
            mContributorName    = itemView.findViewById(R.id.nameTV);;
            mAvatarImageView    = itemView.findViewById(R.id.avatarImage);
        }
    }

    public ContributorListAdapter(Context context, List<ContributorResponse> contributorList )
    {
        mContext            =   context;
        mContributorList    = contributorList;
    }

    @NonNull
    @Override
    public ContributorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_contributor_item, parent, false);

        return new ContributorHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContributorHolder holder, final int position)
    {
        holder.mContributorName.setText(mContributorList.get(position).getmName());

        Glide.with(mContext)
                .load(mContributorList.get(position).getmImageUrl())
                .apply(new RequestOptions().circleCrop())
                .into(holder.mAvatarImageView
                );

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent   = new Intent(mContext, ContributorDetailsActivity.class);
                Bundle bundle  = new Bundle();

                bundle.putSerializable("contributor", mContributorList.get(position));
                intent.putExtras(bundle);

                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (null == mContributorList ||
                0 == mContributorList.size())
            return 0;
        else
            return mContributorList.size();
    }
}
