
package com.example.repobrowser.view.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.repobrowser.R;
import com.example.repobrowser.adapter.ContributorListAdapter;
import com.example.repobrowser.controller.GitRepoController;
import com.example.repobrowser.model.ContributorResponse;
import com.example.repobrowser.model.RepoItem;

import java.util.List;

public class RepoDetailsActivity extends AppCompatActivity
{
    private static final String TAG = RepoDetailsActivity.class.getSimpleName();

    private TextView mNameTV;
    private TextView mLinkTV;
    private TextView mDescTV;

    private ImageView mProfile;
    private ImageView mBackButton;
    private RecyclerView mContributorRecycler;
    private ContributorListAdapter mContributorAdapter;

    private GitRepoController mRepoController;
    private RepoItem mCurrentRepoItem;
    private List<ContributorResponse> mContributorList;

    private ProgressDialog mProgressDialogue;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_details);

        mRepoController = new GitRepoController(this);
        if (getIntent().getExtras() != null)
            mCurrentRepoItem    = (RepoItem) getIntent().getExtras().getSerializable("repoItem");


        initViews();
        initContributorAdapter();
    }

    private void showProgressDialogue()
    {
        mProgressDialogue = new ProgressDialog(this);
        mProgressDialogue.setMessage("Loading..");
        mProgressDialogue.setCanceledOnTouchOutside(false);
        mProgressDialogue.show();

    }

    private void hideProgressDialogue()
    {
        if (null != mProgressDialogue)
        {
            mProgressDialogue.dismiss();
            mProgressDialogue  =  null;
        }
    }

    private void initViews()
    {
        mProfile    = findViewById(R.id.profileImage);
        mNameTV     = findViewById(R.id.nameTV);
        mLinkTV     = findViewById(R.id.linkTV);
        mDescTV     = findViewById(R.id.descTV);
        mBackButton = findViewById(R.id.backButton);

        mNameTV.setText(String.format("%s%s", getString(R.string.profile_name), mCurrentRepoItem.getmFullName()));
        mDescTV.setText(String.format("%s%s", getString(R.string.description), mCurrentRepoItem.getmDescripion()));
        mLinkTV.setText(Html.fromHtml("<font color=#8AFFFFFF>Project Link:- </font> " +
                                            "<font color=#BDB76B>Click Here</font>"));
        mLinkTV.setText("Click Here");
        mLinkTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RepoDetailsActivity.this, ProjectBrowserActivity.class);
                intent.putExtra("url", mCurrentRepoItem.getmHtmlUrl());
                startActivity(intent);
            }
        });

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        RelativeLayout.LayoutParams params  = new RelativeLayout.LayoutParams(displaymetrics.widthPixels, displaymetrics.widthPixels);
        mProfile.setLayoutParams(params);

        Glide.with(this)
                .load(mCurrentRepoItem.getmOwner().getmImageUrl())
                .into(mProfile);


        mContributorRecycler    =   findViewById(R.id.contrRecycler);
        mContributorRecycler.setLayoutManager(new GridLayoutManager(this, 4));
    }



    private void initContributorAdapter()
    {
        showProgressDialogue();
        Log.e(TAG, "url"+mCurrentRepoItem.getmContributorsUrl());
        mRepoController.hitContributorListURL(mCurrentRepoItem.getmContributorsUrl(), new GitRepoController.OnContributorListlListener()
        {
            @Override
            public void onContributorListRecieved(List<ContributorResponse> contributorList)
            {
                mContributorList    = contributorList;

                mContributorAdapter = new ContributorListAdapter(RepoDetailsActivity.this, mContributorList);
                mContributorRecycler.setAdapter(mContributorAdapter);

                hideProgressDialogue();
            }
        });

    }//end initContributorAdapter
}
