package com.example.repobrowser.controller;

import android.content.Context;
import android.util.Log;

import com.example.repobrowser.model.ContributorResponse;
import com.example.repobrowser.model.GitSearchModel;
import com.example.repobrowser.model.RepoItem;
import com.example.repobrowser.network.RepoNetworkManager;
import com.example.repobrowser.network.RepoNetworkStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.android.volley.Request.Method.GET;

public class GitRepoController
{
    private final static String TAG = GitRepoController.class.getSimpleName();

    private Context mContext;
    private RepoNetworkManager mNetworkManager;

    private final static String BASE_URL = "https://api.github.com";
    private final static String REPO = "/search/repositories";
    private final static String COMMITS = "/search/commits";


    public interface OnSearchDetailListener
    {
        void onRepoListReceived(List<RepoItem> repoItemList);
    }

    public interface OnContributorListlListener
    {
        void onContributorListRecieved(List<ContributorResponse> contributorList);
    }


    public interface OnUserRepoListener
    {
        void onUserRepoListReceived(List<RepoItem> repoItemList);
    }

    public GitRepoController(Context context)
    {
        this.mContext   = context;
        mNetworkManager = new RepoNetworkManager(mContext);
    }

    ///
    //
    ///
    public void hitSearchUrl(String queryString, int pageCount, final OnSearchDetailListener listener)
    {
        String url = BASE_URL+REPO+"?q="+queryString;

        Log.e("test", "url "+url);

        /*JSONObject  searchReqObj = new JSONObject();
        try
        {
            searchReqObj.put("q", searchModel.getQString());
            searchReqObj.put("sort", "stars");
            searchReqObj.put("per_page", 10);
            searchReqObj.put("page", pageCount);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "test"+e.toString());
        }
*/
        mNetworkManager.makeJSonObjectRequest(GET, url, null, new RepoNetworkManager.RepoNetworkManagerListener() {
            @Override
            public void onRequestStatusReceived(int statusCode, String response) {
                if (RepoNetworkStatus.OK == statusCode)
                {
                    try
                    {
                        JSONObject searchJson  = new JSONObject(response);
                        JSONArray itemJSONArrray = searchJson.optJSONArray("items");

                        if (itemJSONArrray != null)
                        {
                            List<RepoItem> repoItemList  = new ArrayList<>();
                            for (int index = 0; index < itemJSONArrray.length(); index++)
                            {
                                repoItemList.add(new RepoItem(itemJSONArrray.getJSONObject(index)));
                            }

                            listener.onRepoListReceived(repoItemList);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }//end hitSearchUrl


    ///
    //
    ///
    public void hitContributorListURL(String url, final OnContributorListlListener listener)
    {
        mNetworkManager.makeJSonArrayRequest(GET, url, null, new RepoNetworkManager.RepoNetworkManagerListener() {
            @Override
            public void onRequestStatusReceived(int statusCode, String response) {
                if (RepoNetworkStatus.OK == statusCode)
                {
                    try
                    {
                        JSONArray responseJSONArray = new JSONArray(response);
                        List<ContributorResponse>  contrbutorList = new ArrayList<>();

                        for (int  index = 0; index < responseJSONArray.length(); index++)
                        {
                            contrbutorList.add(new ContributorResponse(responseJSONArray.getJSONObject(index)));

                        }

                        listener.onContributorListRecieved(contrbutorList);

                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });

    }//hitContributorListURL


    ///
    //
    ///
    public void hitContributorRepoDetails(String url, final OnUserRepoListener listener)
    {
        mNetworkManager.makeJSonArrayRequest(GET, url, null, new RepoNetworkManager.RepoNetworkManagerListener() {
            @Override
            public void onRequestStatusReceived(int statusCode, String response) {
                if (RepoNetworkStatus.OK == statusCode)
                {
                    try
                    {
                        JSONArray responseJSONArray  = new JSONArray(response);

                        List<RepoItem> repoItemList  = new ArrayList<>();
                        for (int index = 0; index < responseJSONArray.length(); index++)
                        {
                            repoItemList.add(new RepoItem(responseJSONArray.getJSONObject(index)));
                        }

                        listener.onUserRepoListReceived(repoItemList);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }//end hitContributorDetails



}
