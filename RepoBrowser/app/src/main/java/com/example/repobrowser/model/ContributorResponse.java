package com.example.repobrowser.model;

import org.json.JSONObject;

import java.io.Serializable;

public class ContributorResponse implements Serializable
{
    private String mName;
    private String mImageUrl;
    private String mApiUrl;
    private String mRepoUrl;
    private int mContributionCount;

    public ContributorResponse(JSONObject contributorJSON)
    {
        this.mName      = contributorJSON.optString("login");
        this.mImageUrl  = contributorJSON.optString("avatar_url");
        this.mApiUrl    = contributorJSON.optString("url");
        this.mRepoUrl   = contributorJSON.optString("repos_url");
        this.mContributionCount   = contributorJSON.optInt("contributions");
    }

    public String getmRepoUrl() {
        return mRepoUrl;
    }

    public String getmName() {
        return mName;
    }

    public String getmApiUrl() {
        return mApiUrl;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public int getmContributionCount() {
        return mContributionCount;
    }
}
