package com.example.repobrowser.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class RepoItem implements Serializable
{
    private String mName;
    private String mFullName;
    private String mDescripion;
    private String mHtmlUrl;
    private String mApiUrl;
    private String mContributorsUrl;
    private String mCreatedDate;
    private String mPushedDate;
    private String mUpdatedDate;
    private String mForkCount;
    private String mStagerCount;
    private String mWatcherCount;

    private OwnerResponse mOwner;

    public RepoItem(JSONObject repoItemJson)
    {
        mName               =   repoItemJson.optString("name");
        mFullName           =   repoItemJson.optString("full_name");
        mDescripion         =   repoItemJson.optString("description");
        mHtmlUrl            =   repoItemJson.optString("html_url");
        mApiUrl             =   repoItemJson.optString("url");
        mContributorsUrl    =   repoItemJson.optString("contributors_url");
        mCreatedDate        =   repoItemJson.optString("created_at");
        mPushedDate         =   repoItemJson.optString("pushed_at");
        mUpdatedDate        =   repoItemJson.optString("updated_at");
        mForkCount          =   repoItemJson.optString("forks_count");
        mStagerCount        =   repoItemJson.optString("stargazers_count");
        mWatcherCount       =   repoItemJson.optString("watchers_count");

        try {
            mOwner              =   new OwnerResponse(repoItemJson.getJSONObject("owner"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getmName() {
        return mName;
    }

    public String getmFullName() {
        return mFullName;
    }

    public OwnerResponse getmOwner() {
        return mOwner;
    }

    public String getmApiUrl() {
        return mApiUrl;
    }

    public String getmHtmlUrl() {
        return mHtmlUrl;
    }

    public String getmContributorsUrl() {
        return mContributorsUrl;
    }

    public String getmCreatedDate() {
        return mCreatedDate;
    }

    public String getmDescripion() {
        return mDescripion;
    }

    public String getmForkCount() {
        return mForkCount;
    }

    public String getmPushedDate() {
        return mPushedDate;
    }

    public String getmStagerCount() {
        return mStagerCount;
    }

    public String getmUpdatedDate() {
        return mUpdatedDate;
    }

    public String getmWatcherCount() {
        return mWatcherCount;
    }
}
